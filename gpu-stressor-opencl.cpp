// Copyright (c) 2024 MediaTek, Inc.
// SPDX-License-Identifier: MIT

#define CL_HPP_TARGET_OPENCL_VERSION 200
#include "CL/opencl.hpp"
#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <vector>
#include <regex>

// Define version information
const int VERSION_MAJOR = 1;
const int VERSION_MINOR = 0;
// The GIT_REV macro should be defined during the build process with the current git revision
#ifndef GIT_REV
#define GIT_REV "Unknown"
#endif

// OpenCL kernel - This runs for every work item created.
// For the purpose of stressing, we simply compute
// non-senses with float multiply-add a lot of iterations.
const std::string vectorAddKernelSource = R"(
__kernel void vector_add(__global const float* A, __global const float* B, __global float* C, const uint total_iterations) {
    int idx = get_global_id(0);
    float x = A[idx];
    float y = B[idx];
    float z = C[idx];
    for (uint iter = 0; iter < total_iterations; iter++)
    {
        z = fma(x, y, z);
        z = fma(x, y, z);
        z = fma(x, y, z);
        z = fma(x, y, z);
    }
    C[idx] = z;
}
)";

static inline void report_if_error(cl_int err, int line)
{
	if (err != CL_SUCCESS)
	{
		std::cout << "ERROR line:" << line << "code=[]" << err << "]" << std::endl;
		std::exit(EXIT_FAILURE);
	}
}

#define CHECK(err) report_if_error(err, __LINE__)

int get_mali_gpu_utilization()
{
    int percentage = -1;
    std::ifstream gpuUtilizationFile("/sys/class/misc/mali0/device/gpu_utilization");

    if (!gpuUtilizationFile.is_open()) {
        return -1;
    }
    std::string utilization;
    std::getline(gpuUtilizationFile, utilization);

    // For our MALI GPU kernel driver, the string format looks like this:
    // Mali GPU utilization:57@880000000Hz
    // where "57" is the percentage utilization,
    // and 880000000Hz is the current GPU frequency.
    // Define a regular expression to match the utilization percentage
    std::regex utilizationRegex(R"(.*Mali GPU.*\:(\d+)@\d+Hz)");
    std::smatch matches;

    if (std::regex_search(utilization, matches, utilizationRegex) && matches.size() > 0) {
        std::cout << utilization << std::endl;
        // The first match is the entire pattern.
        // The second match is the 1st match group (percentage)
        std::string percentageStr = matches[1].str();
        try {
            percentage = std::stoi(percentageStr);
            return percentage;
        } catch (const std::invalid_argument& ia) {
            std::cerr << "Invalid argument: " << ia.what() << std::endl;
        } catch (const std::out_of_range& oor) {
            std::cerr << "Out of range error: " << oor.what() << std::endl;
        }
    }

    return -1;
}

void printVersion() {
    std::cout << "genio-stress-gpu version " 
              << VERSION_MAJOR
              << "." << VERSION_MINOR
              << "+g" << GIT_REV
              << std::endl;
}

int main(int argc, char* argv[]) {
    cl_int err = CL_SUCCESS;

    // Parse command-line arguments
    int repeat = -1;
    for (int i = 1; i < argc; ++i) {
        std::string arg = argv[i];
        if ((arg == "-r" || arg == "--repeat") && i + 1 < argc) {
            repeat = std::atoi(argv[++i]); // Convert next argument to integer

        } else if (arg == "-v" || arg == "--version") {
            printVersion();
            return 0;
        }
    }

    // Get platform list
    std::vector<cl::Platform> platforms;
    err = cl::Platform::get(&platforms);
    CHECK(err);

    // Select the first platform and create a context using this platform and the GPU
    cl_context_properties cps[3] = {
        CL_CONTEXT_PLATFORM,
        (cl_context_properties)(platforms[0])(),
        0
    };
    cl::Context context(CL_DEVICE_TYPE_GPU, cps);

    // Get a list of devices on this platform
    std::vector<cl::Device> devices = context.getInfo<CL_CONTEXT_DEVICES>();
    // Always use the first device
    auto device = devices[0];
    std::cout << "OpenCL device: " << device.getInfo<CL_DEVICE_NAME>() << std::endl;

    // Make sure we could print utilization
    if (-1 == get_mali_gpu_utilization()) {
        std::cerr << "MALI GPU node not found. This program only works with MALI GPU on Genio SoC platform." << std::endl;
        return EXIT_FAILURE;
    }

    // Initialize OpenCL input data
    const auto max_item_in_wg = device.getInfo<CL_DEVICE_MAX_WORK_GROUP_SIZE>();
    std::cout << "CL_DEVICE_MAX_WORK_GROUP_SIZE: " << max_item_in_wg << std::endl;
    const size_t GROUP_COUNT = 32768;
    const size_t LIST_SIZE = GROUP_COUNT * max_item_in_wg; // 1M elements
    std::vector<float> A(LIST_SIZE);
    std::vector<float> B(LIST_SIZE);
    std::vector<float> C(LIST_SIZE);
    for (size_t i = 0; i < LIST_SIZE; i++) {
        // Initial values just the make the input different to each other.
        A[i] = static_cast<float>(i);
        B[i] = static_cast<float>(LIST_SIZE - i);
    }

    // Create a command queue
    cl::CommandQueue queue = cl::CommandQueue(context, device, CL_QUEUE_PROFILING_ENABLE);

    // Create memory buffers
    cl::Buffer a_mem_obj = cl::Buffer(context, CL_MEM_READ_ONLY, LIST_SIZE * sizeof(float));
    cl::Buffer b_mem_obj = cl::Buffer(context, CL_MEM_READ_ONLY, LIST_SIZE * sizeof(float));
    cl::Buffer c_mem_obj = cl::Buffer(context, CL_MEM_WRITE_ONLY, LIST_SIZE * sizeof(float));

    // Copy the lists A and B to their respective memory buffers
    err = queue.enqueueWriteBuffer(a_mem_obj, CL_TRUE, 0, LIST_SIZE * sizeof(float), A.data());
    CHECK(err);
    err = queue.enqueueWriteBuffer(b_mem_obj, CL_TRUE, 0, LIST_SIZE * sizeof(float), B.data());
    CHECK(err);

    // Make sure we setup completed before continuing
    err = queue.finish();
    CHECK(err);

    // Create a program from the kernel source
    cl::Program::Sources sources;
    sources.push_back({vectorAddKernelSource.c_str(), vectorAddKernelSource.length()});

    cl::Program program = cl::Program(context, sources);
    err = program.build(devices);
    CHECK(err);

    // Create the OpenCL kernel
    cl::Kernel kernel(program, "vector_add");

    // Set the arguments of the kernel
    err = kernel.setArg(0, a_mem_obj);
    CHECK(err);
    err = kernel.setArg(1, b_mem_obj);
    CHECK(err);
    err = kernel.setArg(2, c_mem_obj);
    CHECK(err);

    // Start from a small iteration in the OpenCL kernel,
    // and increase the interation until the GPU utilization reaches 99% or above
    cl_uint total_iterations = 1024;
    do {
        err = kernel.setArg(3, total_iterations);
        CHECK(err);

        // Execute the OpenCL kernel on the list
        cl::Event profile;
        err = queue.enqueueNDRangeKernel(kernel, cl::NullRange,
                                        cl::NDRange(LIST_SIZE),
                                        cl::NDRange(max_item_in_wg),
                                        NULL,
                                        &profile);
        CHECK(err);
        err = queue.finish();
        CHECK(err);

        // See https://registry.khronos.org/OpenCL/sdk/2.0/docs/man/html/clGetEventProfilingInfo.html
        auto duration_ns = profile.getProfilingInfo<CL_PROFILING_COMMAND_COMPLETE>() - profile.getProfilingInfo<CL_PROFILING_COMMAND_START>();
        std::cout << "time(sec): " << duration_ns / 1000000000.0 << std::fixed << std::setprecision(2) << ", ";
        std::cout << "WorkPerSec: " << LIST_SIZE / (duration_ns / 1000000000.0) << std::fixed << std::setprecision(3) << ", ";

        // get (and print) utilization
        get_mali_gpu_utilization();

        // infinite loop if repeat is -1 (no argument given)
        if (repeat > 0) {
            repeat--;
        }
    } while(repeat != 0);

    return 0;
}