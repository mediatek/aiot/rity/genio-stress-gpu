# To build this program, we recommend using the Yocto Application SDK build environment
# for IoT Yocto v24.0 or later.
# For details, refer to https://mediatek.gitlab.io/aiot/doc/aiot-dev-guide/master/sw/yocto/app-sdk.html

# There are no C++ bindings installed in IoT Yocto's application SDK for rity-demo-image.
# The "opencl-headers" package contains only C header files.
# So we use the local copy of "openCL.hpp".
CXXFLAGS_LOCAL=-I./CL

# Link to libOpenCL.so (system-provided ICD loader).
# This would work on Genio 350/700/1200 that uses MALI GPU.
LDFLAGS_LOCAL=-lOpenCL
TARGET=genio-stress-gpu
SRCS=gpu-stressor-opencl.cpp

# Git revision
GIT_REV = $(shell git rev-parse --short HEAD)

all: $(TARGET)

$(TARGET): $(SRCS)
	$(CXX) $(CXXFLAGS_LOCAL) $(CXXFLAGS) -o $(TARGET) -DGIT_REV=\"$(GIT_REV)\" \
		$(SRCS) $(LDFLAGS_LOCAL) $(LDFLAGS)

clean:
	rm -f $(TARGET)